package main

import "fmt"

func countValleys(trip string, limit int) (valleys int) {
	// init
	trip = trip[:limit]
	altitude := 0
	valleys = 0

	// traverse
	for _, vector := range trip {
		// valley detection
		if vector == 'D' && altitude == 0 {
			valleys += 1
		}

		// altitude update
		if vector == 'D' {
			altitude -= 1
		} else {
			altitude += 1
		}
	}

	return valleys
}

func testValleys() {
	if countValleys("DDUUUUDD", 8) != 1 {
		panic("FAIL")
	}
	if countValleys("DDUUDDUU", 8) != 2 {
		panic("FAIL")
	}
	if countValleys("DDUUDDUU", 6) != 2 {
		panic("FAIL")
	}
	if countValleys("UUDDD", 5) != 1 {
		panic("FAIL")
	}
	if countValleys("UDDUD", 5) != 2 {
		panic("FAIL")
	}
	if countValleys("UDUDUD", 5) != 0 {
		panic("FAIL")
	}
	fmt.Println("PASS")
}

func main() {
    testValleys()
}
