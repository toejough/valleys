def countValleys(trip, limit):
    # init
    trip = trip[:limit]
    altitude = 0
    valleys = 0

    # traverse
    for vector in trip:
        # valley detection
        if altitude == 0 and vector == "D":
            valleys += 1

        # altitude update
        if vector == "D":
            altitude -= 1
        else:
            altitude += 1

    return valleys


def testValleys():
    assert countValleys("DDUUUUDD", 8) == 1
    assert countValleys("DDUUDDUU", 8) == 2
    assert countValleys("DDUUDDUU", 6) == 2
    assert countValleys("UUDDD", 5) == 1
    assert countValleys("UDDUD", 5) == 2
    assert countValleys("UDUDUD", 5) == 0
    print("PASS")

testValleys()
