actor Main
    var env: Env

    new create(env': Env) =>
        env = env'
        testValleys()

    fun countValleys(trip': String, limit: USize): I64 =>
        // init
        var trip_clone = trip'.clone()
        trip_clone.truncate(limit)
        var trip: String box = consume trip_clone
        var altitude: I64 = 0
        var valleys: I64 = 0

        // traverse
        for vector in trip.runes() do
            // valley detection
            if (altitude == 0) and (vector == 'D') then
                valleys = valleys + 1
            end

            // altitude update
            if vector == 'D' then
                altitude = altitude - 1
            else
                altitude = altitude +  1
            end
        end

        valleys

    fun testValleys() =>
        if countValleys("DDUUUUDD", 8) != 1 then
            env.out.print("FAIL")
            return
        end
        if countValleys("DDUUDDUU", 8) != 2 then
            env.out.print("FAIL")
            return
        end
        if countValleys("DDUUDDUU", 6) != 2 then
            env.out.print("FAIL")
            return
        end
        if countValleys("UUDDD", 5) != 1 then
            env.out.print("FAIL")
            return
        end
        if countValleys("UDDUD", 5) != 2 then
            env.out.print("FAIL")
            return
        end
        if countValleys("UDUDUD", 5) != 0 then
            env.out.print("FAIL")
            return
        end
        env.out.print("PASS")
