class Hiker:
    def __init__(self, altimeter, steps_tracker, cartographer):
        # dependencies
        self._altimeter = altimeter
        self._steps_tracker = steps_tracker
        self._cartographer = cartographer

        # logical init
        self._cartographer.update(self._steps_tracker.steps, self._altimeter.altitude)

    def step(self, horizontal, vertical):
        self._steps_tracker.update(horizontal)
        self._altimeter.update(vertical)
        self._cartographer.update(self._steps_tracker.steps, self._altimeter.altitude)

    def get_valley_count(self):
        was_in_valley = False
        valleys = 0
        for point in self._cartographer.map:
            # assess landscape
            in_valley = point.vertical < 0

            # assess difference
            if not was_in_valley and in_valley:
                valleys += 1

            # update landscape
            was_in_valley = in_valley
        return valleys

class Altimeter:
    def __init__(self):
        self.altitude = 0

    def update(self, direction):
        self.altitude += direction

class StepsTracker:
    def __init__(self):
        self.steps = 0

    def update(self, direction):
        self.steps += direction

class Point:
    def __init__(self, horizontal, vertical):
        self.horizontal = horizontal
        self.vertical = vertical

class Cartographer:
    def __init__(self):
        self.map = []

    def update(self, horizontal, vertical):
        self.map.append(Point(horizontal, vertical))

def countValleys(steps, limit):
    # init
    hiker = Hiker(Altimeter(), StepsTracker(), Cartographer())
    direction_translation_map = {
        "D": -1,
        "U": 1,
    }

    # traverse
    for direction in steps[:limit]:
        hiker.step(1, direction_translation_map[direction])

    return hiker.get_valley_count()


def testValleys():
    assert countValleys("DDUUUUDD", 8) == 1
    assert countValleys("DDUUDDUU", 8) == 2
    assert countValleys("DDUUDDUU", 6) == 2
    assert countValleys("UUDDD", 5) == 1
    assert countValleys("UDDUD", 5) == 2
    assert countValleys("UDUDUD", 5) == 0
    print("PASS")

testValleys()
